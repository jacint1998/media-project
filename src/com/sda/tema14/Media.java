package com.sda.tema14;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public abstract class  Media {
    private Double ranking;
    private String name;
    private double rankingSum;
    private int counter;


    public Media(String name) {
        this.name = name;
    };

    public double getRanking() {
        return ranking;
    }

    public void setRanking(Double ranking) {
        this.ranking = ranking;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return " ranking=" + ranking +
                ", name='" + name + '\'' +
                '}';
    }

    public void setRanking(double mark) {
        this.rankingSum = rankingSum+mark;
        counter++;
        this.ranking = rankingSum / counter;
    }

    public void addComment(String comment){
        try {
            FileWriter fileWriter = new FileWriter(String.format("%s.txt",name),true);
            fileWriter.write(String.format("%s \n",comment));
            fileWriter.close();
        }catch(IOException ex){
            ex.printStackTrace();
        }
    }

    public void displayComment(){
        File file = new File(String.format("%s.txt",name));
        try{
        Scanner scanner = new Scanner(file);
        while (scanner.hasNextLine()){
            System.out.println(scanner.nextLine());
        }
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }
    }
}
