package com.sda.tema14;

import java.time.Duration;

public class Music extends MediaLength implements PlayAble {

    String artistName;


    public Music(String artistName, String name, Duration length) {
        super(name, length);
        this.artistName = artistName;
    }

    @Override
    public String toString() {
        return "Music{" +
                "artistName='" + artistName + '\'' +
                super.toString() +
                '}';
    }

    @Override
    public void play() {
        System.out.println("Playing track: " + this.toString());
    }
}
