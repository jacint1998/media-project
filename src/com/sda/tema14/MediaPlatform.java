package com.sda.tema14;

import java.util.ArrayList;
import java.util.List;

public class MediaPlatform {
    List<Media> mediaList = new ArrayList<>();

    public void addMedia(Media media) {
        Media searchMedia = searchByName(media.getName());
        if (searchMedia != null) {
            System.out.println(String.format("media with %s already exist", media.getName()));
        } else {
            this.mediaList.add(media);
        }
    }

    public Media searchByName(String nameOfMedia) {
        for (Media media : mediaList) {
            if (nameOfMedia.equals(media.getName())) {
                return media;
            }
        }
        return null;
    }

    public void searchAndDisplay(String name) {
        Media media = searchByName(name);
        if (media != null) {
            System.out.println(media);
        }
    }

    @Override
    public String toString() {
        return "MediaPlatform{" +
                "mediaList=" + mediaList +
                '}';
    }

    public List<Media> getMediaByType(Class type) {
        List<Media> workingMediaList = new ArrayList<>();
        for (Media media : this.mediaList) {
            if (media.getClass().equals(type)) {
                workingMediaList.add(media);
            }
        }
        return workingMediaList;
    }

    public void searchByAuthor(String name) {
        List<Media> bookList = getMediaByType(Book.class);

        for (Media media : bookList) {
            Book book = (Book) media;
            if (book.getAuthorName().equals(name)) {
                System.out.println(book);
            }
        }

    }
}
