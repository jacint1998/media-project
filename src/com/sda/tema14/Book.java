package com.sda.tema14;

public class Book extends Media implements Readable {
    String authorName;
    int pagesNr;

    public Book(String authorName, String name, int pagesNr) {
        super(name);
        this.authorName = authorName;
        this.pagesNr = pagesNr;
    }

    public String getAuthorName() {
        return authorName;
    }

    @Override
    public String toString() {
        return "Book{" +
                "authorName='" + authorName + '\'' +
                ", pagesNr=" + pagesNr + super.toString() +
                '}';
    }

    @Override
    public void read() {
        System.out.println("Reading " + this);
    }
}
