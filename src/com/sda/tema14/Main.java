package com.sda.tema14;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

/**
 * Definim o aplicatie care o sa fie un catalog pentru diferite tipuri de media(Video, music, books)
 * Video: -name, durata, gen(comedia, actiune, drama), ranking.
 * Music: -artist, nume, durata, ranking.
 * Carti: -autor, nume, nr de pagini, ranking.
 * Constraint: numele trebuie sa fie unice (altfel error message).
 * -utilizatorii pot adauga comentarii la obiecte si aceste vor fi salvate la fisiere text, utilizatorii pot cauta(bazat pe nume),
 * utilizatorii, pot vede comentariile, pentru fiecare tip medie
 * Acest ranking trebuie calculat in functie de notele date.
 **/

public class Main {

    public static void main(String[] args) {
        MediaPlatform mediaPlatform = new MediaPlatform();

        Movie video1 = new Movie("Harry Potter", Duration.ofMinutes(145), VideoType.ACTION, VideoQuality.HD);
        Movie video2 = new Movie("Harry Potter2", Duration.ofMinutes(150), VideoType.ACTION, VideoQuality.HQ);
        video1.setRanking(5);
        video1.setRanking(4);
        video1.setRanking(3.5);
//        System.out.println(video1);

//        video1.addComment("Cea mai tare din parcare!");
//        video1.addComment("E de 5 stele");
//        video1.displayComment();


//        System.out.println("---------------------");
        Music music1 = new Music("Maluma", "HP", Duration.ofMinutes(5));
//        System.out.println(music1);
//        music1.addComment("Piesa e foarte tare");
        music1.displayComment();

        Book book1 = new Book("Liviu Rebreanu", "Ion", 500);
//        System.out.println(book1);
        mediaPlatform.searchAndDisplay("Harry Potter");

        mediaPlatform.addMedia(video1);
        mediaPlatform.addMedia(book1);
        mediaPlatform.addMedia(music1);

        mediaPlatform.getMediaByType(Movie.class);
        mediaPlatform.getMediaByType(Music.class);

        mediaPlatform.searchByAuthor("Liviu Rebreanu");

        YoutubeVideo video3 = new YoutubeVideo("Test", Duration.ofMinutes(2), VideoQuality.HD, 112);


        List<PlayAble> playableObjects = new ArrayList<>();
        playableObjects.add(video3);
        playableObjects.add(video2);
        playableObjects.add(music1);
        playableObjects.add(video1);
        for (PlayAble playAble : playableObjects) {
            playAble.play();
        }
    }
}
