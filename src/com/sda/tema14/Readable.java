package com.sda.tema14;

public interface Readable {
    void read();
}
