package com.sda.tema14;

import java.time.Duration;

public abstract class Video extends MediaLength implements PlayAble {

    VideoQuality videoQuality;

    public Video(String name, Duration length, VideoQuality videoQuality) {
        super(name, length);
        this.videoQuality = videoQuality;
    }

    @Override
    public String toString() {
        return "Video{" +
                "videoQuality=" + videoQuality +
                '}';
    }

}
